import json

import asyncio
import aiohttp
import uvicorn

from urllib.parse import urlparse
from aioflask import render_template, request, escape, Flask, jsonify, redirect
from process import zhihu_data_from_url, bs_scrape_do_fixes, safe_headers
from bs4 import BeautifulSoup

from duckduckgo_search import DDGS

app = Flask(__name__)

######################################################################

@app.template_filter('intsep')
def _jinja2_filter_intsep(i):
        return f'{int(i):,}'

######################################################################

@app.route('/api/v4/questions/<qid>/feeds')
async def api_v4_more_answers(qid):
    arg = dict(request.args)
    url = f'https://www.zhihu.com/api/v4/questions/{qid}/feeds' 
    
    async with aiohttp.ClientSession(headers=safe_headers) as session:
        async with session.get(url, params=arg) as response:
            data = json.loads(await response.text())

            for i in range(len(data['data'])):
                if 'paidInfo' in data['data'][i]['target']:
                    data['data'][i]['target']['content'] = \
                        data['data'][i]['target']['paidInfo']['content']

                soup = BeautifulSoup(data['data'][i]['target']['content'],
                                     'html.parser')
                bs_scrape_do_fixes(soup)
                data['data'][i]['target']['content'] = str(soup)

    # Pointless...
    data.pop('session', None)
    data.pop('ad_info', None) 
    
    return jsonify(data)

@app.route('/answer/<ansid>')
async def api_answer_redir(ansid):
    async with aiohttp.ClientSession(headers=safe_headers) as session:
        async with session.get(f'https://www.zhihu.com/answer/{ansid}',
                               allow_redirects=True) as response:
            return redirect(str(response.url).replace('https://www.zhihu.com',
                                                      ''),
                            code=response.status)



######################################################################

@app.route('/p/<aid>')
@app.route('/p/<aid>')
async def column_view(aid):
    data = await zhihu_data_from_url(f'https://zhuanlan.zhihu.com/p/{aid}')
    column_data = data['entities']['articles'][aid]

    soup = BeautifulSoup(column_data['content'], 'html.parser')
    bs_scrape_do_fixes(soup)

    return await render_template('column.html',
                                 data=column_data,
                                 content=str(soup))

@app.route('/question/<qid>')
@app.route('/question/<qid>')
async def question_view(qid):   # default sort
    data = await zhihu_data_from_url(f'https://www.zhihu.com/question/{qid}')
    question_data = data['entities']['questions'][qid]
    answers_data = data['entities']['answers']
    
    for ansid in answers_data.keys(): # fix the content format.
        if 'paidInfo' in answers_data[ansid]:
                answers_data[ansid]['content'] = \
                        answers_data[ansid]['paidInfo']['content']
        soup = BeautifulSoup(answers_data[ansid]['content'], 'html.parser')
        bs_scrape_do_fixes(soup)
        answers_data[ansid]['content'] = str(soup)

    soup = BeautifulSoup(question_data['detail'], 'html.parser')
    bs_scrape_do_fixes(soup)
    question_data['detail'] = str(soup)
    
    return await render_template('question.html',
                                 qid=qid,
                                 next=data['question']['answers'][qid]['next'],
                                 question=question_data,
                                 answers=list(answers_data.values()))

@app.route('/question/<qid>/answers/updated')
@app.route('/question/<qid>/answers/updated/')
async def question_timed_view(qid):   # time sort
    data = await zhihu_data_from_url(f'https://www.zhihu.com/question/{qid}'
                                     '/answers/updated')
    question_data = data['entities']['questions'][qid]
    answers_data = data['entities']['answers']
    
    for ansid in answers_data.keys(): # fix the content format.
        soup = BeautifulSoup(answers_data[ansid]['content'], 'html.parser')
        bs_scrape_do_fixes(soup)
        answers_data[ansid]['content'] = str(soup)

    answers = list(answers_data.values())
    answers.reverse() # reverse the answers to list by time.

    next_pointer = data['question']['updatedAnswers'][qid]['next']

    soup = BeautifulSoup(question_data['detail'], 'html.parser')
    bs_scrape_do_fixes(soup)
    question_data['detail'] = str(soup)
    
    return await render_template('question.time.html',
                                 qid=qid,
                                 next=next_pointer,
                                 question=question_data,
                                 answers=answers)

@app.route('/question/<qid>/answer/<aid>')
async def question_answer_view(qid, aid):
    data = await zhihu_data_from_url(
        f'https://www.zhihu.com/question/{qid}/answer/{aid}'
    )

    question_data = data['entities']['questions'][qid]
    answers_data = data['entities']['answers']
    
    for ansid in answers_data.keys(): # fix the content format.
        soup = BeautifulSoup(answers_data[ansid]['content'], 'html.parser')
        bs_scrape_do_fixes(soup)
        answers_data[ansid]['content'] = str(soup)

    answers = list(answers_data.values())
    
    soup = BeautifulSoup(question_data['detail'], 'html.parser')
    bs_scrape_do_fixes(soup)
    question_data['detail'] = str(soup)
    
    return await render_template('question.answered.html',
                                 qid=qid,
                                 question=question_data,
                                 answers=answers)

@app.route('/search')
async def search_view():
        kwards = request.args.get('kwards') or '你好世界'
        t = request.args.get('t') or 'column'

        modifer = 'site:zhuanlan.zhihu.com'
        if t == 'column':
                modifer = 'site:zhuanlan.zhihu.com'
        elif t == 'question':
                modifer = 'site:www.zhihu.com/question/'
        
        # with DDGS(proxies='socks5://localhost') as ddgs:
        with DDGS(proxies='socks5://localhost') as ddgs:
                r = list(ddgs.text(f'{modifer} {kwards}',
                                   max_results=20))

        results = []
        for i in r:
                i['title'] = i['title']\
                        .rstrip(' - 知乎 - 知乎专栏') \
                        .rstrip(' - 知乎')
                i['href'] = urlparse(i['href']).path

                if i['href'].startswith('/p/') or \
                   i['href'].startswith('/question/') or \
                   i['href'].startswith('/answer/'):
                        results.append(i)
        
        return await render_template('search.html',
                                     results=results,
                                     kwards=kwards)

@app.route('/people/<anyid>')
async def people_view(anyid):
    data = await zhihu_data_from_url(
        f'https://www.zhihu.com/people/{anyid}'
    )
    user_data = next(iter(data['entities']['users'].values()))

    # return data['entities']
    
    answers_data = data['entities']['answers']
    for ansid in answers_data.keys(): # fix the content format.
        soup = BeautifulSoup(answers_data[ansid]['content'], 'html.parser')
        bs_scrape_do_fixes(soup)
        answers_data[ansid]['content'] = str(soup)
    
    return await render_template('people.html',
                                 data=data['entities'],
                                 user=user_data,
                                 anyid=anyid,
                                 answers=list(answers_data.values()))

######################################################################

if __name__ == '__main__':
    app.run()
