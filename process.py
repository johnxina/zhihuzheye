import json
import aiohttp

from urllib.parse import urlparse, parse_qs, unquote
from bs4 import BeautifulSoup

##
## initialData from zhihu URL
##

safe_headers = { \
    'User-Agent': \
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) '
        'AppleWebKit/537.36 (KHTML, like Gecko) '
        'Chrome/58.0.3029.110 Safari/537.3'
}

async def zhihu_data_from_url(url):
    async with aiohttp.ClientSession(headers=safe_headers) as session:
        async with session.get(url) as response:
            html = await response.text()
            soup = BeautifulSoup(html, 'html.parser')

            js_initial_data = soup.find(id='js-initialData')
            initial_data = json.loads(js_initial_data.string)

            return initial_data['initialState']
        

##
## html beautifulsoup manipulate
##


def bs_del_tag_attrs(tag, attrs=()):
    for attr in attrs:
        del tag[attr]

def bs_scrape_do_fixes(soup):
    bs_cleanup_format(soup)
    bs_cleanup_datas(soup)
    bs_fix_outlink(soup)
    bs_fix_inlink(soup)
    bs_fix_figure(soup)

def bs_cleanup_format(soup):
    for elem in soup.find_all(
            class_=lambda v: v and 'ztext-empty-paragraph' in v
    ):
        elem.decompose()
    
def bs_cleanup_datas(soup):
    kill_attrs = ['data-pid', 'data-size', 'data-original-token',
                  'data-default-watermark-src']

    for kill_attr in kill_attrs:
        elements = soup.find_all(attrs={kill_attr: True})
        for element in elements:
            del element[kill_attr]

def bs_fix_outlink(soup):
    elements = soup.find_all('a', class_='external')

    for element in elements:
        urlp = urlparse(element['href'])
        urlq = parse_qs(urlp.query)
        element['href'] = urlq.get('target', ['/'])[0]

def bs_fix_inlink(soup):
    elements = soup.find_all('a', class_='internal')

    for element in elements:
        strips = ('https://zhuanlan.zhihu.com',
                  'https://www.zhihu.com',
                  'www.zhihu.com',
                  'zhuanlan.zhihu.com')

        if '://' in element['href']:
            element['href'] = element['href'].split('/', 3)
            element['href'] = '/' + element['href'][-1]
        else:
            element['href'] = '/' + element['href'].split('/', 2)[-1]        
            
def bs_fix_figure(soup):
    elements = soup.find_all('img')

    for element in elements:
        if element['src'].startswith('data:image/svg+xml'):
            element.decompose() # Remove the javascript based tags.
        elif element.parent.name == 'noscript':
            element.parent.unwrap() # Flaten the noscript based tags.

    # In case taht BeautifulSoup objects are not updated, we do it again.
    elements = soup.find_all('img')

    for element in elements:
        src = element['src']
        temp = BeautifulSoup(f'<span class="f"><img src="{ src }" /></span>',
                             'html.parser')
        
        element['tabindex'] = 1
        element['loading'] = 'lazy'
        element.insert_after(temp.span)
