from duckduckgo_search import DDGS

ddgs_opts = {
    'proxies': 'socks5://localhost:7891',
}

with DDGS(**ddgs_opts) as ddgs:
    for i in ddgs.text('site:zhuanlan.zhihu.com/p/ Lorem Ipsum'):
        print(i)
