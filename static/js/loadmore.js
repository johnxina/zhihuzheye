// @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPL-3.0
// Show the load more button.

if (document.getElementById('js-hidden'))
    document.getElementById('js-hidden').style = 'display: none;'

jsld = document.getElementById('js-only')
jsld.style = ''

function load_more() {
    var xhr = new XMLHttpRequest()
    xhr.open("GET", window.next_pointer, true)

    xhr.onreadystatechange = function () {
	if (xhr.readyState === 4 && xhr.status === 200) {
	    var d = JSON.parse(xhr.responseText)

	    d['data'].forEach(function (i, idx) {
		it = i['target']
		document.getElementById('answer-body').innerHTML += `<div class="post"><div></div><header class="bar-nav"><img src="${it.author.avatar_url}"><div><a href="/people/${it.author.id}"><div class="title">${it.author.name}</div></a><div class="description">${it.author.headline}</div></div></header><div>${it.content}</div></div>`
	    })

	    window.next_pointer = d['paging']['next'].replace('https://www.zhihu.com', '')
	    
	    if (d['paging'].is_end) {
		jsld.innerHTML = '<div>没有更多的回答</div>'
	    }	    
	} else if (xhr.readyState === 4 && xhr.status !== 200) {
	    jsld.innerHTML = '<div>请求错误，无法继续：</div>' + xhr.status 
	}
    }

    xhr.send()
}

// @license-end
